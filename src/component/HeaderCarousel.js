import React, {Component} from 'react';
import Slider from "react-slick";
import {getText} from "../locales";

class HeaderCarousel extends Component {
    render() {
        const settings = {
            dots: true,
            nav: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
        };
        return (

            <div className="header-carousel">
                <Slider {...settings}>
                    <div className="item">
                        <div className="d-flex justify-content-center">
                            <img src="images/partner.png" alt=""/>
                        </div>
                        <div className="text">
                            {getText('carouselTitle')}
                        </div>
                    </div>
                    <div className="item">
                        <div className="d-flex justify-content-center">
                            <img src="images/partner.png" alt=""/>
                        </div>
                        <div className="text">
                            {getText('carouselTitle')}
                        </div>
                    </div>
                    <div className="item">
                        <div className="d-flex justify-content-center">
                            <img src="images/partner.png" alt=""/>
                        </div>
                        <div className="text">
                            {getText('carouselTitle')}
                        </div>
                    </div>
                </Slider>
            </div>
        );
    }
}

export default HeaderCarousel;