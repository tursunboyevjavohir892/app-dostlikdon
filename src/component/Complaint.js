import React from 'react';

function Complaint(props) {
    return (
        <div className="main">
            <div className="tick">
                <img src="images/tick.png" alt=""/>
            </div>
            <div className="title">
                <div>Shikoyatlar mavjudmi?</div>
                <div>onlayn tarzda yuboring</div>
            </div>
            <div className="send">
                <button type="button" className="btn btn-success">Murojaat yuborish</button>
            </div>
            <div className="planshet">
                <img src="images/planshet.png" alt=""/>
            </div>
        </div>
    );
}

export default Complaint;