import React from 'react';
import {Link} from 'react-router-dom'

function NotFound(props) {
    return (
        <div className="container">
            <div className="row vh-100 align-items-center">
                <div className="col-6 offset-3  ">

                    <img className="img-fluid w-100" src="images/notFound.png" alt=""/>
                    <h3 className="text-center">
                        <Link className="text-decoration-none text-dark"
                    to="/">Bosh sahifaga o'tish</Link></h3>
                </div>
            </div>

        </div>
    );
}

export default NotFound;