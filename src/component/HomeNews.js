import React, {useEffect} from 'react';
import Slider from "react-slick";
import {getText} from "../locales";

function News(props) {

    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 2,
        centerMode: true,
        centerPadding: '-5px',
    };

    return (
        <div className={'section-second'}>
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className={'main-title'}>
                            {getText('news')}
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className={'col-md-12'}>
                        <Slider {...settings}>
                            <div className={'report-carousel'}>
                                <div className="box-shadow">
                                    <div className={'top-img'}>
                                        <img src="images/mark.png" alt="mark"/>
                                    </div>
                                    <div className="report-body">
                                        <div className={'info-date'}>
                                            <div>
                                                <img src="images/date.png" alt="date"/>
                                                <span className={'ml-1'}>16:48 / 12.11.20</span>
                                            </div>
                                            <div>
                                                <img src="images/eye.png" alt="eye"/>
                                                <span className={'ml-1'}>321</span>
                                            </div>
                                            <div>
                                                <img src="images/message.png" alt="maessage"/>
                                                <span className={'ml-1'}>100</span>
                                            </div>
                                        </div>
                                        <div className={'report-ad'}>
                                            Eksport qiluvchi tadbirkorlik
                                            <div>subektlariga...</div>
                                        </div>
                                        <div className={'reportBody-title'}>
                                            "DO`STLIKDONMAXSULOTLARI" AJ ning
                                            aksiyadorlari diqqatiga! 2020-yil 27-mart
                                            kuni soat 9:00 dan...
                                        </div>
                                        <div className={'report-btn'}>
                                            Batafsil
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className={'report-carousel'}>
                                <div className="box-shadow">
                                    <div className={'top-img'}>
                                        <img src="images/people.svg" alt="people"/>
                                    </div>
                                    <div className="report-body">
                                        <div className={'info-date'}>
                                            <div>
                                                <img src="images/date.png" alt="date"/>
                                                <span className={'ml-1'}>16:48 / 12.11.20</span>
                                            </div>
                                            <div>
                                                <img src="images/eye.png" alt="eye"/>
                                                <span className={'ml-1'}>321</span>
                                            </div>
                                            <div>
                                                <img src="images/message.png" alt="maessage"/>
                                                <span className={'ml-1'}>100</span>
                                            </div>
                                        </div>
                                        <div className={'report-ad'}>
                                            E’LON !!!
                                        </div>
                                        <div className={'reportBody-title'}>
                                            "DO`STLIKDONMAXSULOTLARI" AJ ning
                                            aksiyadorlari diqqatiga! 2020-yil 27-mart
                                            kuni soat 9:00 dan...
                                        </div>
                                        <div className={'report-btn'}>
                                            Batafsil
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className={'report-carousel'}>
                                <div className="box-shadow">
                                    <div className={'top-img'}>
                                        <img src="images/Symbol.svg" alt="Symbol"/>
                                    </div>
                                    <div className="report-body">
                                        <div className={'info-date'}>
                                            <div>
                                                <img src="images/date.png" alt="date"/>
                                                <span className={'ml-1'}>16:48 / 12.11.20</span>
                                            </div>
                                            <div>
                                                <img src="images/eye.png" alt="eye"/>
                                                <span className={'ml-1'}>321</span>
                                            </div>
                                            <div>
                                                <img src="images/message.png" alt="maessage"/>
                                                <span className={'ml-1'}>100</span>
                                            </div>
                                        </div>
                                        <div className={'report-ad'}>
                                            E’LON !!!
                                        </div>
                                        <div className={'reportBody-title'}>
                                            "DO`STLIKDONMAXSULOTLARI" AJ ning
                                            aksiyadorlari diqqatiga! 2020-yil 27-mart
                                            kuni soat 9:00 dan...
                                        </div>
                                        <div className={'report-btn'}>
                                            Batafsil
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className={'report-carousel'}>
                                <div className="box-shadow">
                                    <div className={'top-img'}>
                                        <img src="images/people.svg" alt="people"/>
                                    </div>
                                    <div className="report-body">
                                        <div className={'info-date'}>
                                            <div>
                                                <img src="images/date.png" alt="date"/>
                                                <span className={'ml-1'}>16:48 / 12.11.20</span>
                                            </div>
                                            <div>
                                                <img src="images/eye.png" alt="eye"/>
                                                <span className={'ml-1'}>321</span>
                                            </div>
                                            <div>
                                                <img src="images/message.png" alt="maessage"/>
                                                <span className={'ml-1'}>100</span>
                                            </div>
                                        </div>
                                        <div className={'report-ad'}>
                                            E’LON !!!
                                        </div>
                                        <div className={'reportBody-title'}>
                                            "DO`STLIKDONMAXSULOTLARI" AJ ning
                                            aksiyadorlari diqqatiga! 2020-yil 27-mart
                                            kuni soat 9:00 dan...
                                        </div>
                                        <div className={'report-btn'}>
                                            Batafsil
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className={'report-carousel'}>
                                <div className="box-shadow">
                                    <div className={'top-img'}>
                                        <img src="images/Symbol.svg" alt="Symbol"/>
                                    </div>
                                    <div className="report-body">
                                        <div className={'info-date'}>
                                            <div>
                                                <img src="images/date.png" alt="date"/>
                                                <span className={'ml-1'}>16:48 / 12.11.20</span>
                                            </div>
                                            <div>
                                                <img src="images/eye.png" alt="eye"/>
                                                <span className={'ml-1'}>321</span>
                                            </div>
                                            <div>
                                                <img src="images/message.png" alt="maessage"/>
                                                <span className={'ml-1'}>100</span>
                                            </div>
                                        </div>
                                        <div className={'report-ad'}>
                                            E’LON !!!
                                        </div>
                                        <div className={'reportBody-title'}>
                                            "DO`STLIKDONMAXSULOTLARI" AJ ning
                                            aksiyadorlari diqqatiga! 2020-yil 27-mart
                                            kuni soat 9:00 dan...
                                        </div>
                                        <div className={'report-btn'}>
                                            Batafsil
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Slider>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default News;