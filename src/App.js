import {BrowserRouter, Route, Switch} from 'react-router-dom'
import Home from "./pages/Home";
import Login from "./pages/Login";
import Info from "./pages/Info";
import {ToastContainer} from "react-toastify";
import NotFound from "./component/NotFound";
import PrivateRoute from "./component/PrivateRoute";
import AdminMenus from "./component/AdminMenus";
import AdminNews from "./component/AdminNews";
function App() {
    return (
        <div>
            <BrowserRouter>
                <Switch>
                    <Route path="/" exact component={Home}/>
                    <Route path="/login" exact component={Login}/>
                    <PrivateRoute path="/info" exact component={Info}/>
                    <PrivateRoute path="/admin" exact component={AdminNews}/>
                    <PrivateRoute path="/admin/menu" exact component={AdminMenus}/>
                    <Route component={NotFound}/>
                </Switch>
            </BrowserRouter>
            <ToastContainer/>
        </div>
    );
}

export default App;
