import {UPDATE_STATE} from "../actionType/actionType";

const initialState={
    modalOpen: false,
    isSubMenu: false,
    generatedUrl: "",
    menus: [],
    deleteModalOpen: false,
    selectedIdForDelete: null,
    selectedMenu: {},
};
export const adminMenusReducer=(state=initialState,action)=>{
    switch (action.type) {
        case UPDATE_STATE :
            state={
                ...state,
                ...action.payload
            }
    }
    return state
};