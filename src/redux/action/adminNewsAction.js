import {UPDATE_STATE} from "../actionType/actionType";



export function updateState (data){
    return {
        type: UPDATE_STATE,
        payload: data
    }
}
